<?php 
	$d->reset();
	$sql = "select noidung$lang as noidung,photo from #_about where type='thongtin' limit 0,1";
	$d->query($sql);
	$company_thongtin = $d->fetch_array();

    $d->reset();
    $sql="select ten$lang as ten,tenkhongdau,id,photo,mota$lang as mota from #_product_danhmuc where hienthi=1 and type='san-pham' order by stt,id desc";
    $d->query($sql);
    $product_danhmuc=$d->result_array();  	
?>

<div class="row1 row_ab">
	<div class="col-md-6 col-sm-6 col-xs-12 col-tt-ab">
		<h1><?=$tintuc_detail['ten']?></h1>
		<div class="pad_content">
			<?=$tintuc_detail['noidung']?>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12 col-img-ab lazy" data-src="<?=_upload_hinhanh_l.$tintuc_detail['photo']?>"></div>
</div>


<div class="wap_dm_ab lazy" data-src="images/bg_ab_sp.jpg">
	<div class="slick_ab_sp">
		<?php foreach ($product_danhmuc as $v) {?>
		<div>
			<div class="pxx_ab">
				<h2><a href="san-pham/<?=$v['tenkhongdau']?>"><?=$v['ten']?></a></h2>
				<div><?=$v['mota']?></div>
			</div>
		</div>
		<?php }?>
	</div>
</div>

<div class="wap_bot_ab">
	<div class="wapper">
		<div class="row">
			<div class="col-ms-6 col-sm-6 col-xs-12 col-b-ab">
				<h2>Thông tin công ty</h2>
				<div class="pad_content">
					<?=$tintuc_detail['noidung']?>
				</div>
			</div>
			<div class="col-ms-6 col-sm-6 col-xs-12 col-b-ab1">
				<img src="<?=_upload_hinhanh_l.$company_thongtin['photo']?>" alt="Thông tin công ty">
			</div>
		</div>
	</div>
</div>