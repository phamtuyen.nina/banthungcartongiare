<div class="hb-image-bg-wrap lazy" data-src="<?=_upload_sanpham_l.$title_bar['banner']?>">
    <div class="wapper">
        <h1 class="hb-animate-element wow fadeInLeft"><?=$title_cat?></h1>
    </div>
</div>
<div class="breadcrumb">
    <div class="wapper">
        <?=$bread->display();?>
    </div>
</div>
<div class="wapper">
    <div class="show_pr row1">
        <?php foreach ($product as $v) {?>
        <div class="col-md-3 col-sm-4 col-xs-6 col-pp wow fadeInDown">
            <div class="pad_product ">
                <a href="san-pham/<?=$v['tenkhongdau']?>.html">
                    <span>Xem chi tiết</span>
                    <img src="thumb/300x300x2x100/<?=_upload_sanpham_l.$v['photo']?>" alt="<?=$v['ten']?>">
                </a>
                <div class="info_pr">
                    <a href="san-pham/<?=$v['tenkhongdau']?>.html">
                        <h2><?=$v['ten']?></h2>
                    </a>
                </div>
            </div>
        </div>
        <?php }?>
    </div>
    <div class="clear"></div>
    <div class="pagination"><?=pagesListLimitadmin($url_link , $totalRows , $pageSize, $offset)?></div>
</div>