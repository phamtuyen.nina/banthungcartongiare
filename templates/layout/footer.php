<?php	

	$d->reset();
	$sql_contact = "select noidung$lang as noidung from #_about where type='footer' limit 0,1";
	$d->query($sql_contact);
	$company_contact = $d->fetch_array();

    $d->reset();
    $sql_product_danhmuc="select ten$lang as ten,tenkhongdau,id from #_product_danhmuc where hienthi=1 and type='san-pham' order by stt,id desc";
    $d->query($sql_product_danhmuc);
    $product_danhmuc=$d->result_array();

    $d->reset();
    $sql = "select ten$lang as ten,link,photo from #_slider where hienthi=1 and type='xahoi' order by stt,id desc";
    $d->query($sql);
    $lienket=$d->result_array();    
?>
<div id="wap_footer" class="lazy" data-src="images/bg_bot.jpg">
    <div class="wapper">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 col-main1">
                <p class="tt_ft">Thông tin liên hệ</p>
                <?=$company_contact['noidung'];?>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 col-main2">
                <p class="tt_ft">Sản phẩm dịch vụ</p>
                <ul>
                    <?php foreach ($product_danhmuc as $v) {?>
                    <li>
                        <a href="san-pham/<?=$v['tenkhongdau']?>"><?=$v['ten']?></a>
                    </li>
                    <?php }?>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 col-main3">
                <p class="tt_ft">Kết nối với chúng tôi</p>
                <div id="lienket">
                    <?php foreach ($lienket as $v) {?>
                    <a href="<?=$v['link']?>" target="_blank">
                        <img src="<?=_upload_hinhanh_l.$v['photo']?>" alt="<?=$v['ten']?>">
                    </a>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right">
        <div class="wapper">
            © 2019 — <?=$company['ten']?>. All right reserved.
        </div>
    </div>
</div>
