
<h1 hidden="true"><?=$title_cat?></h1>
<div class="wap_liwnhw_ row1">
    <div class="col-ll_l col-sm-8 col-xs-12">
        <div class="pb_ff-l">
            <div class="frm_lienhe">
                <form method="post" name="frm" class="frm" action="lien-he.html" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6 col-input">
                            <p><?=_hovaten?> <span>(*)</span></p>
                            <input name="ten_lienhe" type="text" id="ten_lienhe" />
                        </div>
                         <div class="col-sm-6 col-xs-6 col-input">
                            <p><?=_diachi?> <span>(*)</span></p>
                            <input name="diachi_lienhe" type="text" id="diachi_lienhe" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-6 col-input">
                            <p><?=_dienthoai?> <span>(*)</span></p>
                            <input name="dienthoai_lienhe" type="text" id="dienthoai_lienhe" />
                        </div>
                         <div class="col-sm-6 col-xs-6 col-input">
                            <p>Email <span>(*)</span></p>
                            <input name="email_lienhe" type="text" id="email_lienhe" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12 col-input">
                            <p><?=_chude?> <span>(*)</span></p>
                            <input name="tieude_lienhe" type="text" id="tieude_lienhe" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12 col-input">
                            <p><?=_noidung?> <span>(*)</span></p>
                            <textarea name="noidung_lienhe" id="noidung_lienhe" rows="5"></textarea>
                        </div>
                        <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12 col-input">
                            <button type="button" class="click_ajax"><?=_gui?></button>
                            <button type="button" onclick="document.frm.reset();"><?=_nhaplai?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-ll_r col-sm-4 col-xs-12">
        <div class="pbb_la">
             <?=$company_contact['noidung'];?>
        </div>
    </div>
</div>
<div class="maps">
    <?=$company['googlemap']?>
</div>