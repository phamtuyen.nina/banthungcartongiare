<?php 
    $d->reset();
    $sql = "select ten$lang as ten,mota$lang as mota from #_about where type='about' limit 0,1";
    $d->query($sql);
    $about_contact = $d->fetch_array();

    $d->reset();
    $sql="select ten$lang as ten,tenkhongdau,id,photo,mota$lang as mota from #_product_danhmuc where hienthi=1 and type='san-pham' order by stt,id desc";
    $d->query($sql);
    $product_danhmuc=$d->result_array();    

    $d->reset();
    $sql = "select id,ten$lang as ten,tenkhongdau,mota$lang as mota from #_news where type='dich-vu' and hienthi=1 order by stt,id desc";
    $d->query($sql);
    $dichvu = $d->result_array();   

    $d->reset();
    $sql = "select photo from #_background where type='dichvu' limit 0,1";
    $d->query($sql);
    $row_dv = $d->fetch_array();   

    $d->reset();
    $sql = "select ten$lang as ten,link,photo,mota from #_slider where hienthi=1 and type='taisao' order by stt,id desc";
    $d->query($sql);
    $taisao=$d->result_array();    

    $d->reset();
    $sql = "select noidung$lang as noidung from #_about where type='taisao' limit 0,1";
    $d->query($sql);
    $company_taisao = $d->fetch_array();

    $d->reset();
    $sql="select ten$lang as ten,tenkhongdau,id,photo from #_product where hienthi=1 and type='san-pham' and noibat=1 order by stt,id desc";
    $d->query($sql);
    $product=$d->result_array();  
?>

<div class="wap_about">
    <div class="wapper">
        <div class="titlle_ab wow fadeInUp">
            <h1><?=$about_contact['ten']?></h1>
        </div>
        <div class="mm_about wow fadeInUp">
            <?=$about_contact['mota']?>
        </div>
    </div>
</div>
<div class="line_key">
    <p><i class="fa fa-angle-double-down" aria-hidden="true"></i></p>
</div>
<div class="wwap_dm_pr">
    <div class="wapper">
        <div class="titlle_ab wow fadeInDown">
            <p>Sản phẩm</p>
        </div>
        <div class="row">
            <div class="slick_dmsp">
                <?php foreach ($product_danhmuc as $key => $v) {?>
                <div class="col-xs-4">
                    <div class="col-paf">
                        <a href="san-pham/<?=$v['tenkhongdau']?>">
                            <img class=" wow flipInY" src="<?=_upload_sanpham_l.$v['photo']?>" alt="<?=$v['ten']?>">
                        </a>
                        <div class="in_ff  wow flipInY">
                            <h2>
                                <a href="san-pham/<?=$v['tenkhongdau']?>"><?=$v['ten']?></a>
                            </h2>
                            <div><?=$v['mota']?></div>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>

<div class="wap_dv lazy" data-src="images/bg_dv.jpg">
    <div class="wapper">
        <div class="row">
            <div class="col-md-8 wow fadeInRight">
                <img src="<?=_upload_hinhanh_l.$row_dv['photo']?>" alt="Dịch vụ">
            </div>
            <div class="col-md-4">
                <div class="all_ser">
                    <h3 class="wow fadeInUp" data-wow-delay="0.2s">Dịch vụ</h3>
                    <?php foreach ($dichvu as $v) {?>
                    <div class="box_ser">
                        <h4 class="wow fadeInUp" data-wow-delay="0.5s"><?=$v['ten']?></h4>
                        <div class="wow fadeInUp" data-wow-delay="0.3s"><?=$v['mota']?></div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wap_spnoibat">
    <div class="wapper">
        <div class="titlle_ab wow fadeInDown">
            <h2>Sản phẩm nổi bật</h2>
        </div>
        <div class="show_pr row1">
            <?php foreach ($product as $v) {?>
            <div class="col-md-3 col-sm-4 col-xs-6 col-pp  wow fadeInDown">
                <div class="pad_product">
                    <a href="san-pham/<?=$v['tenkhongdau']?>.html">
                        <span>Xem chi tiết</span>
                        <img src="thumb/300x300x2x100/<?=_upload_sanpham_l.$v['photo']?>" alt="<?=$v['ten']?>">
                    </a>
                    <div class="info_pr">
                        <a href="san-pham/<?=$v['tenkhongdau']?>.html">
                            <h2><?=$v['ten']?></h2>
                        </a>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</div>
<div class="wap_taisao">
    <div class="wapper">
        <div class="tt_taisao wow fadeInUp">
            <h3>Tại sao chọn chúng tôi ?</h3>
        </div>
        <div class="mm_taisao wow fadeInDown">
            <?=$company_taisao['noidung']?>
        </div>
        <div class="row wow fadeInUp">
            <div class="slick_taisao">
                <?php foreach ($taisao as $v) {?>
                <div class="col-xs-6">
                    <div class="bb_ts">
                        <div class="img_ts">
                            <img src="<?=_upload_hinhanh_l.$v['photo']?>" alt="<?=$v['ten']?>">
                        </div>
                        <h5><?=$v['ten']?></h5>
                        <div class="mm_ts"><?=$v['mota']?></div>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>