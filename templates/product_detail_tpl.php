<link href="magiczoomplus/magiczoomplus.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="css/tab.css" type="text/css" rel="stylesheet" />
<div class="breadcrumb">
    <div class="wapper">
        <?=$bread->display();?>
    </div>
</div>
<?php 
    $d->reset();
    $sql="select ten$lang as ten,tenkhongdau,id,photo from #_product where hienthi=1 and type='san-pham' and noibat=1 order by stt,id desc limit 0,5";
    $d->query($sql);
    $product1=$d->result_array();  
?>
<div class="wapper">
    <div class="row">
        <div class="col-md-10 col-sm-8 col-xs-12 col-dl-pro">
            <div class="box_container">
                <div class="wap_pro clearfix">
                    <div class="zoom_slick">
                        <div class="slick2">
                            <a data-zoom-id="Zoom-detail" id="Zoom-detail" class="MagicZoom" href="<?php if($row_detail['photo'] != NULL)echo _upload_sanpham_l.$row_detail['photo'];else echo 'images/noimage.gif';?>" title="<?=$row_detail['ten']?>"><img class='cloudzoom' src="<?php if($row_detail['photo'] != NULL)echo _upload_sanpham_l.$row_detail['photo'];else echo 'images/noimage.gif';?>" /></a>
                            <?php $count=count($hinhthem); if($count>0) {?>
                            <?php for($j=0,$count_hinhthem=count($hinhthem);$j<$count_hinhthem;$j++){?>
                                <a data-zoom-id="Zoom-detail" id="Zoom-detail" class="MagicZoom" href="<?php if($hinhthem[$j]['photo']!=NULL) echo _upload_hinhthem_l.$hinhthem[$j]['photo']; else echo 'images/noimage.gif';?>" title="<?=$row_detail['ten']?>" ><img src="<?php if($hinhthem[$j]['photo']!=NULL) echo _upload_hinhthem_l.$hinhthem[$j]['photo']; else echo 'images/noimage.gif';?>" /></a>
                            <?php }} ?>
                        </div>
                        <?php $count=count($hinhthem); if($count>0) {?>
                        <div class="slick">
                            <p><img src="thumb/100x80x2x90/<?php if($row_detail['photo'] != NULL)echo _upload_sanpham_l.$row_detail['photo'];else echo 'images/noimage.gif';?>" /></p>
                            <?php for($j=0,$count_hinhthem=count($hinhthem);$j<$count_hinhthem;$j++){?>
                                <p><img src="thumb/100x80x2x90/<?php if($hinhthem[$j]['thumb']!=NULL) echo _upload_hinhthem_l.$hinhthem[$j]['thumb']; else echo 'images/noimage.gif';?>" /></p>
                            <?php } ?>
                        </div>
                        <?php } ?>
                    </div>
                    <ul class="product_info">
                        <li class="ten"><h1><?=$row_detail['ten']?></h1></li>
                        <li class="gia"><b>Giá:</b> <span><?php if($row_detail['gia'] != 0)echo number_format($row_detail['gia'],0, ',', '.').' <sup>đ</sup>';else echo 'Liên hệ'; ?></span></li>
                        <li><b>Min Order:</b> <span><?=$row_detail['minorder']?></span></li>
                        <li><b>Liên hệ tư vấn:</b> <span><a href="http://zalo.me/<?=preg_replace('/[^0-9]/','',$company['dienthoai'])?>"><?=$company['dienthoai']?></a></span></li>
                        <li><div class="addthis_native_toolbox"></div></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div id="tabs">
                    <ul id="ultabs">
                        <li data-vitri="0"><?=_thongtinsanpham?></li>
                    </ul>
                    <div style="clear:both"></div>
                    <div id="content_tabs">
                        <div class="tab">
                            <?=$row_detail['noidung']?>
                        </div>
                        <div class="fb-comments" data-href="<?=getCurrentPageURL()?>" data-numposts="5" data-width="100%"></div>
                    </div>
                </div>
            </div>
            <?php if(!empty($product)){ ?>
            <div class="wap_spnoibat wap_spnoibat_cungloai">
                <div class="wapper">
                    <div class="titlle_ab wow fadeInDown">
                        <h2>Sản phẩm cùng loại</h2>
                    </div>
                    <div class="show_pr row1">
                    <?php foreach ($product as $v) {?>
                    <div class="col-md-3 col-sm-3 col-xs-6 col-pp wow fadeInDown">
                        <div class="pad_product ">
                            <a href="san-pham/<?=$v['tenkhongdau']?>.html">
                                <span>Xem chi tiết</span>
                                <img src="thumb/300x300x2x100/<?=_upload_sanpham_l.$v['photo']?>" alt="<?=$v['ten']?>">
                            </a>
                            <div class="info_pr">
                                <a href="san-pham/<?=$v['tenkhongdau']?>.html">
                                    <h2><?=$v['ten']?></h2>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div>
                <div class="clear"></div>
                <div class="pagination"><?=pagesListLimitadmin($url_link , $totalRows , $pageSize, $offset)?></div>
                </div>
            </div>
            <?php }?>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-12 col-dlr-pro">
            <h2>Sản phẩm nổi bật</h2>
            <div class="show_pr row1">
            <?php foreach ($product1 as $v) {?>
            <div class="col-md-12 col-sm-3 col-xs-6 col-pp  wow fadeInDown">
                <div class="pad_product">
                    <a href="san-pham/<?=$v['tenkhongdau']?>.html">
                        <span>Xem chi tiết</span>
                        <img src="thumb/300x300x2x100/<?=_upload_sanpham_l.$v['photo']?>" alt="<?=$v['ten']?>">
                    </a>
                    <div class="info_pr">
                        <a href="san-pham/<?=$v['tenkhongdau']?>.html">
                            <h2><?=$v['ten']?></h2>
                        </a>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
        </div>
    </div>
</div>
